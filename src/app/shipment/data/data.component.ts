import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class  DataComponent implements OnInit, OnChanges {
  @Input() isBounceIn = true;
  @Input() isFadeOut = false;
  @Input() isAnimated = false;
  @Input() position: string;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }



}
