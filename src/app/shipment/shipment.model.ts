import {Status} from './status/status.model';

export class Shipment {
  public id: number;
  public name: string;
  public bookingReference: string;
  public carrierName: string;
  public steps: Array<Status>;
  constructor(id: number, name: string, bookingReference: string, carrierName: string, steps: Array<Status>) {
    this.id = id;
    this.name = name;
    this.bookingReference = bookingReference;
    this.carrierName = carrierName;
    this.steps = steps;
  }
}
