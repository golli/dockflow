export class Status {
  public id: number;
  public vesselName: string;
  public time: string;
  public type: string;
  public locationCountry: string;
  public locationCity: string;
  public container: string;
  constructor(id: number, vesselName: string, time: string, type: string, locationCountry: string, locationCity: string,container:string) {
    this.id = id;
    this.vesselName = vesselName;
    this.time = time;
    this.type = type;
    this.locationCountry = locationCountry;
    this.locationCity = locationCity;
    this.container = container;
  }
}
