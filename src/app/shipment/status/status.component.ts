import {Component, OnInit, Input, EventEmitter, Output, ElementRef} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Status} from './status.model';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css'],
  animations: [
    trigger('divState', [
      state('normal', style({
        display: 'none',
        opacity: 0,
        transform: 'scale(1)'

      })),
      state('highlighted', style({
        display: 'block',
        opacity: 1,
        transform: 'scale(1)'

      })),
      transition('normal => highlighted', [
        animate('300ms ease-in-out')
      ]),
      transition('highlighted => normal', [
        animate('300ms ease-in-out')
      ])
    ])
    ]
})
export class StatusComponent implements OnInit {
  @Input() hasHovered = true;
  @Input() hideOnExit = false;
  @Output() removeSelected = new EventEmitter();
  @Output() setHovered = new EventEmitter();
  @Output() showBounce = new EventEmitter<{'ComponentPosition': number}>();
  @Output() dropDownFollow = new EventEmitter<{'ComponentPosition': number}>();
  @Input() step: Status;
  state = 'normal';
  constructor(private el: ElementRef) { }

  ngOnInit() {
  }
  // Show user the requested component/checkpoint

  onMouseEnter() {
    if (this.state === 'normal') {
      // hide all checkpoints(status)

      this.removeSelected.emit();
      // update animation for the new selected checkpoint(status)
      this.state = 'highlighted';

      if (this.hasHovered === false) {
        this.setHovered.emit();
        // Update the background of data component
        this.showBounce.emit(this.el.nativeElement.offsetLeft);
      } else {
        // Update the background of data component
        this.dropDownFollow.emit(this.el.nativeElement.offsetLeft);
      }
    }
  }



}
