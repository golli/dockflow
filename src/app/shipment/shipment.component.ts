import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, Input,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {StatusComponent} from './status/status.component';
import {DataComponent} from './data/data.component';
// @ts-ignore
import Demo_shipment from '../../assets/REST_TEST.json';
import {Shipment} from './shipment.model';
import {Status} from './status/status.model';
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.css']
})
export class ShipmentComponent implements AfterViewInit {
  hideOnExit = false;
  steps: Status[] = [];
  tradeFlow: Shipment;

  @ViewChildren(StatusComponent) statsComponents!: QueryList<StatusComponent>;
  @ViewChild(DataComponent, {static: false}) dropdownComponent;
  constructor(private cd: ChangeDetectorRef) {
    // create and initialise our models using JSON file
    for ( const  step of Demo_shipment.sea_shipments[0].sea_movements) {
      this.steps.push(new Status(step.vessel_telo_loading.id,
        Demo_shipment.sea_shipments[0].carrier.name,
        formatDate(step.vessel_telo_loading.readings[0].reading, 'dd/MM/yyyy hh:mm a z', 'en-US'),
        step.vessel_telo_loading.readings[0].type,
        step.vessel_telo_loading.location.name,
        step.vessel_telo_loading.location.raw_location.un_country,
        step.transport_unit_sub_shipments[0].transport_unit.reference
      ));
      this.steps.push(new Status(step.vessel_telo_discharge.id,
        Demo_shipment.sea_shipments[0].carrier.name,
        formatDate(step.vessel_telo_discharge.readings[0].reading, 'dd/MM/yyyy hh:mm a z', 'en-US'),
        step.vessel_telo_discharge.readings[0].type,
        step.vessel_telo_discharge.location.name,
        step.vessel_telo_discharge.location.raw_location.un_country,
        step.transport_unit_sub_shipments[0].transport_unit.reference
      ));
    }
    this.tradeFlow = new Shipment(Demo_shipment.id,
      Demo_shipment.name,
      Demo_shipment.sea_shipments[0].booking_reference,
      Demo_shipment.sea_shipments[0].carrier.name,
      this.steps
    );

  }
  // display first checkpoint/step of the shipment
  ngAfterViewInit() {
    this.statsComponents.first.state = 'highlighted';
    this.cd.detectChanges();
  }
  // User select didn't select any checkpoint to display
  onMouseLeave() {
    if (this.hideOnExit) {
      this.statsComponents.forEach(statusInstance => statusInstance.hasHovered = false);
      hideDropDown();
    }
    function hideDropDown() {
      this.statsComponents.forEach(statusInstance => statusInstance.state = 'normal');
      this.dropdownComponent.isBounceIn = false;
      this.dropdownComponent.isFadein = true;
    }
  }
  onRemoveSelected() {
    this.statsComponents.forEach(statusInstance => statusInstance.state = 'normal');
  }
  // update the background for the selected checkpoint(put it behind the required component)
  showBounce(pos) {
    this.dropdownComponent.position = (pos - 88).toString();
    this.dropdownComponent.isfadeOut = false;
    this.dropdownComponent.isBounceIn = true;
    this.dropdownComponent.isAnimated = true;

  }
  // update the background for the selected checkpoint(put it behind the required component)
  onDropDownFollow(pos) {
    this.dropdownComponent.position = (pos - 88).toString();
  }
  onHovered() {
    this.statsComponents.forEach(statusInstance => statusInstance.hasHovered = true);
  }

}
